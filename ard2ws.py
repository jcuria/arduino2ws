import json
import ConfigParser
import urllib2
import signal
import sys
import serial
import time
from datetime import datetime


def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


def checkConfigFields(url, frequency, port):	
	msg = ''	
	if not url:
		msg = "URL cannot be empty. Please, enter a correct URL in the config file."
	elif not frequency:
		msg = "Frequency cannot be empty. Please, enter a frequency in minutes."
	elif not port:
		msg = "Port cannot be empty. Please, enter a corret port where Arduino is connected."	
	if msg != '':
		sys.exit('Error: ' + msg)
		

def getTemperature(sensor):
	temperature = sensor[sensor.index('=') + 1:]	
	return float(temperature)


def sendDataToWebService(temperature):	
	currentDateTime = datetime.now()
	date = currentDateTime.strftime('%Y/%m/%d')
	time = currentDateTime.strftime('%H:%M:%S')

	### Create json object ###
	data = json.JSONEncoder().encode({		
		"temperature": temperature,
		"date": date,
		"time": time
	})

	try:
		req = urllib2.Request(url.geturl(), data, {'Content-Type': 'application/json'})
		f = urllib2.urlopen(req)
		response = f.read()
		f.close()

		data = json.loads(response)
	
		if int(data['code']) > 0:
			print 'Error: ' + date + '\t' + time + '\t' + (data['message']).encode('utf-8') + '\n'
			return False
				
	except:
		print 'Error conneting to server.'
		return False
	
	return True


def getLanguage():
	if len(sys.argv) > 1:
		if (sys.argv[1]).startswith('ca'):
			language = 'ca_ES'
		else:	
			language = sys.argv[1]
	else:
		language = 'en'		
	return language	


def signal_handler(signal, frame):
	print '\nClosing the application...'
	sys.exit(0)


def checkParameters():
	if len(sys.argv) > 2:
		sys.exit('Invalid execution of script.\nUsage: python ard2ws.py [en|ca|es]')
        

######################## Main code ########################
checkParameters()

print '******** Initializing Arduino2WS service *******'
print '***** Press [Ctrl + C] to stop the service *****'

signal.signal(signal.SIGINT, signal_handler)

# Get all data info from Config file
Config = ConfigParser.ConfigParser()
Config.read("config.ini")

url 		= ConfigSectionMap("Restful Web Service")['url']
frequency	= ConfigSectionMap("Restful Web Service")['frequency']
port		= ConfigSectionMap("Config")['port']

# Set language to WS
language = getLanguage()

checkConfigFields(url, frequency, port)

frequency = float(frequency) * 60

try:
	from urlparse import urlparse
except ImportError:
	from urllib.parse import urlparse

url = urlparse(url + '?lang=' + language)

try:
	ser = serial.Serial(port, 9600)
except:
	sys.exit('Error: Could not open port ' + port + '. Make sure Arduino is connected to device and the port is correct.')

nvalues_received = 0
temperature_total = 0
start = time.time()

while True:
	end = time.time()
	
	if (end - start) >= frequency:
		print 'Sending data to Web Service...'
		
		if nvalues_received > 0:				
			sendDataToWebService(temperature_total / nvalues_received)				
			nvalues_received = 0
			temperature_total = 0
			
		start = time.time()

	sensor = ser.readline()

	if "Temperature" in sensor:
		temperature = getTemperature(sensor)
		temperature_total = temperature_total + temperature
		nvalues_received = nvalues_received + 1
