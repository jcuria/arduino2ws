**Arduino2WS**
is a simple python script to receive data from Arduino temperature sensor and send it to a Restful web service.

**Usage:**
python ard2ws.py [en|ca|es]

An optional locale can be specified to be sent to the WS if the WS can return localized messages.

In the **Config** file you must specify the **url** of the WS, the **frequency** in minutes to send data to the WS and the **port** where Arduino is connected.

**Arduino**

Arduino Sample format line: Temperature=20

Temperature sensor used: DS18B20 waterproof

